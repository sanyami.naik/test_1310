import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");
        String fName=req.getParameter("fName");
        String lName=req.getParameter("lName");
        Long number=Long.parseLong(req.getParameter("number"));
        int age=Integer.parseInt(req.getParameter("age"));
        String email=req.getParameter("email");
        String city=req.getParameter("city");
        System.out.println(fName+lName+number+age+email+city);

        Cookie cookie=new Cookie("fname",fName);
        resp.addCookie(cookie);

        Connection connection=ConnectionDB.connect();

        try {
            PreparedStatement preparedStatement= connection.prepareStatement("insert into reg values(?,?,?,?,?,?);");
            preparedStatement.setString(1,fName);
            preparedStatement.setString(2,lName);
            preparedStatement.setLong(3,number);
            preparedStatement.setInt(4,age);
            preparedStatement.setString(5,email);
            preparedStatement.setString(6,city);

            preparedStatement.execute();


            resp.sendRedirect("login.html");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
