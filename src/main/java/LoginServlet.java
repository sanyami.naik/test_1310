import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");

        String lName=req.getParameter("lEmail");
        String password=req.getParameter("lPassword");

        Connection connection=ConnectionDB.connect();

        if(lName.equals("admin@gmail.com")&& password.equals("555"))
        {
            printWriter.println("Welcome "+lName);
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("admin.html");
            requestDispatcher.forward(req,resp);
        }
        else {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("Select number from reg where email='" + lName + "';");

                if (resultSet.next()) {
                    String tablepass = resultSet.getString(1);
                    System.out.println(tablepass);
                    if (password.equals(tablepass)) {

                        resultSet = statement.executeQuery("Select * from reg where email='" + lName + "';");
                        while (resultSet.next()) {
                            Cookie[] ck = req.getCookies();
                            String name = (String) ck[1].getValue();
//                        printWriter.println("Welcome "+name);
                            printWriter.println("<center><table border='2'>");
                            printWriter.println("<tr><th>USERNAME</th><th>LASTNAME</th><th>NUMBER</th><th>AGE</th><th>EMAIL</th><th>CITY</th></tr>");

                            printWriter.println("<tr><td>" + resultSet.getString(1) + "</td><td>" + resultSet.getString(2) + "</td><td>" + resultSet.getInt(3) + "</td><td>" + resultSet.getInt(4) + "</td><td>" + resultSet.getString(5) + "</td><td>" + resultSet.getString(6) + "</td></tr>");


                        }
                        printWriter.println("<br><a href='login.html'>Click here to logout</a>");

                    } else {
                        printWriter.println("<h1>Invalid username or password</h1>");
                        RequestDispatcher requestDispatcher = req.getRequestDispatcher("login.html");
                        requestDispatcher.include(req, resp);
                    }
                }


            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }
    }
