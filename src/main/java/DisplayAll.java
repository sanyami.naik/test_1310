import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/DisplayAll")
public class DisplayAll extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection connection=ConnectionDB.connect();
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");

        try {
        Statement statement = connection.createStatement();
        ResultSet resultSet ;

        resultSet = statement.executeQuery("Select * from reg;");
            printWriter.println("<center><table border='2'>");
            printWriter.println("<tr><th>USERNAME</th><th>LASTNAME</th><th>NUMBER</th><th>AGE</th><th>EMAIL</th><th>CITY</th></tr>");

            while (resultSet.next()) {


                printWriter.println("<tr><td>" + resultSet.getString(1) + "</td><td>" + resultSet.getString(2) + "</td><td>" + resultSet.getInt(3) + "</td><td>" + resultSet.getInt(4) + "</td><td>" + resultSet.getString(5) + "</td><td>" + resultSet.getString(6) + "</td></tr>");


            }
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("admin.html");
            requestDispatcher.include(req,resp);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }



        }
    }

